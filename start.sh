#!/bin/bash

if [ $1 == "testing" -o $1 == "development" -o $1 == "staging" -o $1 == "pre-production" -o $1 == "production" ]
then
	sh scripts/build.sh $1
	sh scripts/deploy.sh $1
else
	echo "Please enter one of the following - testing, development, staging, pre-production, production"
fi