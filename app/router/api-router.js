/**
 * Copyright - Panally Internet
 */


/******************
 *
 *
 * Flow of the router config, () signifies next in the heirarchy
 * 
 * api-router --> (static-router) --> (main-router)
 *
 *
 *******************/


// Imports
const express = require('express');
const fs = require('fs');    
const path = require('path');
const request = require('superagent');

const Url = require('app/config/core/url');

// Initializing the router
const router = express.Router();

router.get('/api/auth/:username/:password', function(req, res) {
	request
	.get(Url.Api + 'auth/get/' + req.params.username + '/' + req.params.password)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.get('/api/business/:businessId', function(req, res) {
	request
	.get(Url.Api + 'business/get/' + req.params.businessId)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.get('/api/index/:businessId', function(req, res) {
	request
	.get(Url.Api + 'core/index/' + req.params.businessId)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.post('/api/register', function(req, res) {
	request
	.post(Url.Api + 'core/insert/register')
	.send(req.body)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.post('/api/validate-ref', function(req, res) {
	request
	.post(Url.Api + 'core/validate/referral')
	.send(req.body)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.post('/api/referral', function(req, res) {
	request
	.post(Url.Api + 'core/insert/referral')
	.send(req.body)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.post('/api/validate-redeem', function(req, res) {
	request
	.get(Url.Api + 'core/validate/redeem/' + req.body.businessId + '/' + req.body.phoneNumber)
	.end(function(error, response){
		res.json(response.body);
	})
});


router.post('/api/redeem', function(req, res) {
	request
	.post(Url.Api + 'core/insert/redeem')
	.send(req.body)
	.end(function(error, response){
		res.json(response.body);
	})
});


module.exports = router;
