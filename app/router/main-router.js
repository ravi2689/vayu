/**
 * Copyright - Panally Internet
 */


/******************
 *
 *
 * Flow of the router config, () signifies next in the heirarchy
 * 
 * api-router --> static-router --> main-router
 *
 *
 *******************/


// Import external libraries
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const ReactRouter = require('react-router');
const Helmet = require('react-helmet');
const request = require('superagent');
const StyleSheetServer = require('aphrodite').StyleSheetServer;
const fs = require('fs');

require('babel-core/register')({
	presets: ['es2015', 'react']
});
require.extensions['.css'] = () => {
  return;
};

const match = ReactRouter.match;
const RouterContext = React.createFactory(ReactRouter.RouterContext);
const Provider = React.createFactory(require('react-redux').Provider);

const compressor = require('app/utils/es6-compressor');
const logger = require('app/utils/logger');
const Constants = require('app/config/core/constants');
const Keys = require('app/config/core/keys');
const Url = require('app/config/core/url');

const routes = require('app/config/routes.js').routes;
const router = require('app/router/static-router');

const AssetName = JSON.parse(fs.readFileSync('manifest.json'));

const commonJs = Url.Static.Js + AssetName['common.js'];

router.get('/', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
	let store = require('app/pages/index/redux-store');
	const indexUrl = Url.Fetch.Index + req.user.businessId;
	request
	.get(indexUrl)
	.end(function(error, response){
		if (error) {
			//TODO
		}
		let initialStateData = response.body;
		initialStateData.user = req.user;
		const initialState = {
			data : initialStateData,
		}
		store = store.configureStore(initialState);
		match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
			if (error) {
				res.status(500).send(error.message)
			} else if (redirectLocation) {
				res.redirect(302, redirectLocation.pathname + redirectLocation.search)
			} else if (renderProps) {
				const preloadedState = store.getState();
				const head = Helmet.rewind();
				const pageJs = Url.Static.Js + AssetName['index.js'];
				const webPage = compressor`
					<!doctype html>
					<html>
						<head>
						</head>
						<body>
							<div id="react-mount"></div>
							<script>
							  window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
							</script>
							<script src=${commonJs}></script>
							<script src=${pageJs}></script>
							<script src="https://cdn.smooch.io/smooch.min.js"></script>
							<script>
								Smooch.init({appToken: '${Keys.SmoochIo}'});
							</script>
						</body>
					</html>
				`;
				res.header('Content-Type', 'text/html');
				res.write(webPage);
				res.end();
			} else {
				res.status(404).send('Not found');
			}
		});    
	}); 
});


router.get('/register', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
	let store = require('app/pages/register/redux-store');
	let initialStateData = {};
	initialStateData.user = req.user;
	const initialState = {
		data : initialStateData,
	}
	store = store.configureStore(initialState);
	match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const preloadedState = store.getState();
			const head = Helmet.rewind();
			const pageJs = Url.Static.Js + AssetName['register.js'];
			const webPage = compressor`
				<!doctype html>
				<html>
					<head>
					</head>
					<body>
						<div id="react-mount"></div>
						<script>
						  window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
						</script>
						<script src=${commonJs}></script>
						<script src=${pageJs}></script>
						<script src="https://cdn.smooch.io/smooch.min.js"></script>
						<script>
							Smooch.init({appToken: '${Keys.SmoochIo}'});
						</script>
					</body>
				</html>
			`;
			res.header('Content-Type', 'text/html');
			res.write(webPage);
			res.end();
		} else {
			res.status(404).send('Not found');
		}
	});
});


router.get('/referral', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
	let store = require('app/pages/referral/redux-store');
	let initialStateData = {};
	initialStateData.user = req.user;
	const initialState = {
		data : initialStateData,
	}
	store = store.configureStore(initialState);
	match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const preloadedState = store.getState();
			const head = Helmet.rewind();
			const pageJs = Url.Static.Js + AssetName['referral.js'];
			const webPage = compressor`
				<!doctype html>
				<html>
					<head>
					</head>
					<body>
						<div id="react-mount"></div>
						<script>
						  window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
						</script>
						<script src=${commonJs}></script>
						<script src=${pageJs}></script>
						<script src="https://cdn.smooch.io/smooch.min.js"></script>
						<script>
							Smooch.init({appToken: '${Keys.SmoochIo}'});
						</script>
					</body>
				</html>
			`;
			res.header('Content-Type', 'text/html');
			res.write(webPage);
			res.end();
		} else {
			res.status(404).send('Not found');
		}
	});
});


router.get('/redeem', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
	let store = require('app/pages/redeem/redux-store');
	let initialStateData = {};
	initialStateData.user = req.user;
	const initialState = {
		data : initialStateData,
	}
	store = store.configureStore(initialState);
	match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const preloadedState = store.getState();
			const head = Helmet.rewind();
			const pageJs = Url.Static.Js + AssetName['redeem.js'];
			const webPage = compressor`
				<!doctype html>
				<html>
					<head>
					</head>
					<body>
						<div id="react-mount"></div>
						<script>
						  window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
						</script>
						<script src=${commonJs}></script>
						<script src=${pageJs}></script>
						<script src="https://cdn.smooch.io/smooch.min.js"></script>
						<script>
							Smooch.init({appToken: '${Keys.SmoochIo}'});
						</script>
					</body>
				</html>
			`;
			res.header('Content-Type', 'text/html');
			res.write(webPage);
			res.end();
		} else {
			res.status(404).send('Not found');
		}
	});
});


router.get('/contact',  require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
	let store = require('app/pages/contact/redux-store');
	let initialStateData = {};
	initialStateData.user = req.user;
	const initialState = {
		data : initialStateData,
	}
	store = store.configureStore(initialState);
	match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const preloadedState = store.getState();
			const head = Helmet.rewind();
			const pageJs = Url.Static.Js + AssetName['contact.js'];
			const webPage = compressor`
				<!doctype html>
				<html>
					<head>
					</head>
					<body>
						<div id="react-mount"></div>
						<script>
						  window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
						</script>
						<script src=${commonJs}></script>
						<script src=${pageJs}></script>
					</body>
				</html>
			`;
			res.header('Content-Type', 'text/html');
			res.write(webPage);
			res.end();
		} else {
			res.status(404).send('Not found')
		}
	});
});

const massMessageJson = require('app/utils/message/massMessageJson');
const singleMessageJson = require('app/utils/message/singleMessageJson');
const otpJson = require('app/utils/message/otpJson');
const refSucessJson = require('app/utils/message/refSucessJson');

router.post('/register', function(req, res) {
	const registerUrl = Url.Post.Register;
	request
	.post(registerUrl)
	.send(req.body)
	.end(function(error, response){
		if (error) {
			return error;
		}
		if (!response.body){
			return null;
		}
		// SEND MESSAGES
		const messageUrl = Url.MessageUrl;
		const generatedJson = massMessageJson(response.body);
		request
		.post(messageUrl)
		.set('authkey', Keys.Msg91)
		.set('Content-Type', 'application/json')
		.send(generatedJson)
		.end(function(error, messageResponse){
			if (error) {
				// TODO
			}
			if (messageResponse && !messageResponse.text){
				// TODO
			}
			res.status(200).send('Ok');
		});
	});
});

router.post('/validate-ref', function(req, res) {
	const validateRefUrl = Url.Post.ValidateRef;
	request
	.post(validateRefUrl)
	.send(req.body)
	.end(function(error, messageResponse){
		if (error) {
			return error;
		}
		if (messageResponse.body == ''){
			return res.status(401).send();
		}
		// SEND OTP
		const messageUrl = Url.OtpUrl;
		const generatedJson = otpJson(messageResponse.body);
		request
		.get(messageUrl)
		.query(generatedJson)
		.end(function(error, messageResponse){
			if (error) {
				// TODO
			}
			if (messageResponse && !messageResponse.body){
				// TODO
			}
			res.status(200).send('Ok');
		});
	});
});

// If successful, 3 messages should be triggered
router.post('/referral', function(req, res) {
	const referralUrl = Url.Post.Referral;
	request
	.post(referralUrl)
	.send(req.body)
	.end(function(error, response){
		if (error) {
			return error;
		}
		if (response.body == '' || response.body.businessId == ''){
			return res.status(401).send();
		}
		// SEND MESSAGE TO NEW USER
		const messageUrl = Url.MessageUrl;
		const generatedJson = singleMessageJson(response.body);
		request
		.post(messageUrl)
		.set('authkey', Keys.Msg91)
		.set('Content-Type', 'application/json')
		.send(generatedJson)
		.end(function(error, messageResponse){
			if (error) {
				// TODO
			}
			if (messageResponse && !messageResponse.body){
				// TODO
			}
			// SEND MESSAGE TO REFERRER
			const messageUrl = Url.MessageUrl;
			const generatedJson = refSucessJson(response.body);
			request
			.post(messageUrl)
			.set('authkey', Keys.Msg91)
			.set('Content-Type', 'application/json')
			.send(generatedJson)
			.end(function(error, messageResponse){
				if (error) {
					// TODO
				}
				if (messageResponse && !messageResponse.body){
					// TODO
				}
				res.status(200).send('Ok');
			});
		});
	});
});


router.post('/validate-redeem', function(req, res) {
	const ValidateRedeemUrl = Url.Post.ValidateRedeem;
	request
	.post(ValidateRedeemUrl)
	.send(req.body)
	.end(function(error, messageResponse){
		if (error) {
			return error;
		}
		if (messageResponse.body == ''){
			return res.status(401).send();
		}
		// SEND OTP
		const messageUrl = Url.OtpUrl;
		const generatedJson = otpJson(messageResponse.body);
		request
		.get(messageUrl)
		.query(generatedJson)
		.end(function(error, messageResponse){
			if (error) {
				// TODO
			}
			if (messageResponse && !messageResponse.body){
				// TODO
			}
			res.status(200).send('Ok');
		});
	});
});


router.post('/redeem', function(req, res) {
	const redeemUrl = Url.Post.Redeem;
	request
	.post(redeemUrl)
	.send(req.body)
	.end(function(error, response){
		if (error) {
			return error;
		}
		if (response.body == '' || response.body.businessId == ''){
			return res.status(401).send();
		}
		res.status(200).send('Ok');
	});
});


router.get('/login', function(req, res) {
	match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const head = Helmet.rewind();
			const pageJs = Url.Static.Js + AssetName['login.js'];
			const webPage = compressor`
				<!doctype html>
				<html>
					<head>
					</head>
					<body>
						<div id="react-mount"></div>
						<script src=${commonJs}></script>
						<script src=${pageJs}></script>
						<script src="https://cdn.smooch.io/smooch.min.js"></script>
						<script>
							Smooch.init({appToken: '${Keys.SmoochIo}'});
						</script>
					</body>
				</html>
			`;
			res.header('Content-Type', 'text/html');
			res.write(webPage);
			res.end();
		} else {
			res.status(404).send('Not found');
		}
	});
});


module.exports = router;
