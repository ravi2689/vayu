
/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {blue600, grey900} from 'material-ui/styles/colors';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

export default getMuiTheme({
	fontFamily: 'Roboto, sans-serif',
	 palette: {
	  },
	  appBar: {
	    height: 57,
	    color: blue600
	  },
	  drawer: {
	    width: 230,
	    color: grey900
	  },
	  raisedButton: {
	    primaryColor: blue600,
	  },
	userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36',
});