/**
 * Copyright - Panally Internet
 */

import React from 'react'
import Helmet from 'react-helmet'

class Include extends React.Component {
	render() {
		return (
			<Helmet
				title="Contact Us"
				meta={[
					{name: "description", content: "Contact us @ Panally"},
				]}
			/>
		);
	}
}

module.exports = Include;
