/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import {cyan600, pink600, purple600, orange600} from 'material-ui/styles/colors'
import Assessment from 'material-ui/svg-icons/action/assessment'
import Face from 'material-ui/svg-icons/action/face'
import ThumbUp from 'material-ui/svg-icons/action/thumb-up'
import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart'

import InfoBox from 'app/containers/index/InfoBox'
import DailyRegistrations from 'app/containers/index/DailyRegistrations'
import DailyReferrals from 'app/containers/index/DailyReferrals'
import CurrentCampaigns from 'app/containers/index/CurrentCampaigns'
import RecentRegistrations from 'app/containers/index/RecentRegistrations'

import 'app/assets/css/flexboxgrid.min.css'

import globalStyles from 'app/styles'

class DashboardPage extends React.Component {

	render() {
		let referralFactor = 0;
		return (
			<div>
				<h3 style={globalStyles.navigation}>Application / Dashboard</h3>

				<div className="row">

					<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
						<InfoBox Icon={ShoppingCart}
							color={pink600}
							title="Total Members"
							value={this.props.data.totalMembers}
						/>
					</div>
					<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
						<InfoBox Icon={ThumbUp}
							color={cyan600}
							title="New Members"
							value={this.props.data.newRegistrations}
						/>
					</div>
					<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
						<InfoBox Icon={Assessment}
							color={purple600}
							title="Conversions"
							value={this.props.data.conversions}
						/>
					</div>
					<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
						<InfoBox Icon={Face}
							color={orange600}
							title="Conversion Factor"
							value={this.props.data.conversionFactor}
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-md m-b-15">
						<DailyRegistrations data={this.props.data.dailyRegistrations}/>
					</div>

					<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 m-b-15">
						<DailyReferrals data={this.props.data.dailyReferrals}/>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
						<RecentRegistrations data={this.props.data.recentRegistrations}/>
					</div>
					<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
						<CurrentCampaigns data={this.props.data.currentCampaign}/>
					</div>
				</div>
			</div>
		)
	}
}

export default DashboardPage
