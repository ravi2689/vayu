/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */
 
import React from 'react'
import {connect}  from 'react-redux'

import Login from 'app/pages/login/Login'
import Include from 'app/pages/login/Include'


class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<Login/>
			</div>
		);
	}
}

module.exports = Page;