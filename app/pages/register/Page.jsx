/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */
 
import React from 'react'
import {connect}  from 'react-redux'

import Register from 'app/pages/register/Register'
import Include from 'app/pages/register/Include'

class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<Register
					data={this.props.data}
				/>
			</div>
		);
	}
}

const PageState = function(state){
	return {
		data: state.data
	}
};

Page = connect(
	PageState
)(Page);

module.exports = Page;