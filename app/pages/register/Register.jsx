/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'

import request from 'superagent'

import Form from 'app/containers/register/Form'

import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

import { SubmissionError } from 'redux-form'

import 'app/assets/css/redux-form.css'

const reducer = combineReducers({
  form: reduxFormReducer
})

const store = createStore(reducer)

class Register extends React.Component {

	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onSubmit(value){
		let registerData = value;
		registerData.businessId = this.props.data.user.businessId;
		request
			.post('/register')
			.send(registerData)
			.set('Accept', 'application/json')
			.end(function(error, response){
				if (error) {
					// TODO
				}
				else{
					window.location.replace('/');
				}
			}
		);
	}

	render() {
		return (
			<Provider store={store}>
				<div>
					<Form
						onSubmit={this.onSubmit}
					/>
				</div>
			</Provider>
		)
	}
}

export default Register;
