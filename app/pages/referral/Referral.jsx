/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'

import request from 'superagent'

import Form from 'app/containers/referral/Form'

import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

import { SubmissionError } from 'redux-form'

import 'app/assets/css/redux-form.css'

const reducer = combineReducers({
  form: reduxFormReducer
})

const store = createStore(reducer);

class Validate extends React.Component {

	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
		this.generateOtp = this.generateOtp.bind(this);
	}

	generateOtp(value){
		let validityData = value;
		validityData.businessId = this.props.data.user.businessId;
		return request
		.post('/validate-ref')
		.send(validityData)
		.set('Accept', 'application/json')
		.catch((error) => {
			throw new SubmissionError({
				_error: 'Invalid Referral. Please try again'
			})
		})
	}

	onSubmit(value){
		let validityData = value;
		validityData.businessId = this.props.data.user.businessId;
		return request
		.post('/referral')
		.send(validityData)
		.set('Accept', 'application/json')
		.catch((error, response) => {
			if(error){
				throw new SubmissionError({
					_error: 'Otp Mismatch. Please try again'
				})
			}
		})
	}

	render() {
		return (
			<Provider store={store}>
				<div>
					<Form
						generateOtp={this.generateOtp}
						onSubmit={this.onSubmit}
					/>
				</div>
			</Provider>
		)
	}
}

export default Validate;
