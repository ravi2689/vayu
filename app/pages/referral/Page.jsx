/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */
 
import React from 'react'
import {connect}  from 'react-redux'

import Referral from 'app/pages/referral/Referral'
import Include from 'app/pages/referral/Include'


class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<Referral
					data={this.props.data}
				/>
			</div>
		);
	}
}

const PageState = function(state){
	return {
		data: state.data
	}
};

Page = connect(
	PageState
)(Page);

module.exports = Page;