/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'

import request from 'superagent'

import Form from 'app/containers/redeem/Form'

import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

import 'app/assets/css/redux-form.css'

const reducer = combineReducers({
  form: reduxFormReducer
})

const store = createStore(reducer)

class Redeem extends React.Component {

	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
		this.generateOtp = this.generateOtp.bind(this);
	}

	generateOtp(value){
		let validityData = value;
		validityData.businessId = this.props.data.user.businessId;
		return request
		.post('/validate-redeem')
		.send(validityData)
		.set('Accept', 'application/json')
		.catch((error) => {
			throw new SubmissionError({
				_error: 'Invalid Redeem. Please try again'
			})
		})
	}

	onSubmit(value){
		let validityData = value;
		validityData.businessId = this.props.data.user.businessId;
		return request
		.post('/redeem')
		.send(validityData)
		.set('Accept', 'application/json')
		.catch((error, response) => {
			if(error){
				throw new SubmissionError({
					_error: 'Otp Mismatch. Please try again'
				})
			}
		})
	}

	render() {
		return (
			<Provider store={store}>
				<div>
					<Form
						generateOtp={this.generateOtp}
						onSubmit={this.onSubmit}
					/>
				</div>
			</Provider>
		)
	}
}

export default Redeem;
