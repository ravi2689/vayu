/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */
 
import React from 'react'
import {connect}  from 'react-redux'

import Redeem from 'app/pages/redeem/Redeem'
import Include from 'app/pages/redeem/Include'


class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<Redeem
					data={this.props.data}
				/>
			</div>
		);
	}
}

const PageState = function(state){
	return {
		data: state.data
	}
};

Page = connect(
	PageState
)(Page);

module.exports = Page;