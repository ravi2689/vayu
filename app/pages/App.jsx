/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import muiTheme from 'app/pages/mui-theme'
import Include from 'app/pages/Include'

import Keys from 'app/config/core/keys'
import Url from 'app/config/core/url'
import CoreStyle from  'app/config/core/style'

import Header from 'app/components/Header';
import LeftDrawer from 'app/components/LeftDrawer';
import withWidth, {LARGE, SMALL} from 'material-ui/utils/withWidth';
import Data from 'app/data';

import {connect}  from 'react-redux'

import ReactGA from'react-ga';

class App extends React.Component {

	logAppView() {
		ReactGA.set({ page: window.location.pathname + window.location.search });
		ReactGA.pageview(window.location.pathname + window.location.search);
	}

	constructor(props) {
		super(props);
		this.state = {
			showChildren: false,
			prebootHTML: this.props.params.prebootHTML,
			navDrawerOpen: true
		};
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.width !== nextProps.width) {
			this.setState({navDrawerOpen: nextProps.width === LARGE});
		}
	}

	handleChangeRequestNavDrawer() {
		this.setState({
			navDrawerOpen: !this.state.navDrawerOpen
		});
	}

	componentDidMount() {
		this.setState({ prebootHTML: '', showChildren: true })
		ReactGA.initialize(Keys.GoogleAnalytics);
		this.logAppView();
	};

	render() {
		
		let { navDrawerOpen } = this.state;
		const paddingLeftDrawerOpen = 236;

		const styles = {
			header: {
				paddingLeft: navDrawerOpen ? paddingLeftDrawerOpen : 0
			},
			container: {
				margin: '80px 20px 20px 15px',
				paddingLeft: navDrawerOpen && this.props.width !== SMALL ? paddingLeftDrawerOpen : 0
			}
		};
		return (
			<div>
				<Include/>
				<MuiThemeProvider muiTheme={muiTheme}>
					<div>
						<Header 
							styles={styles.header}
							 handleChangeRequestNavDrawer={this.handleChangeRequestNavDrawer.bind(this)}
						/>
						<LeftDrawer 
							navDrawerOpen={navDrawerOpen}
							menus={Data.menus}
							name={this.props.data.user.businessName}
						/>
						<div style={styles.container}>
							{this.props.children}
						</div>
					</div>
				</MuiThemeProvider>
			</div>
		)
	}
}

const AppState = function(state){
	return {
		data: state.data
	}
};

App = connect(
	AppState
)(App);

export default withWidth()(App);
