/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react';
import { StyleSheet, css } from 'aphrodite'

import {Link} from 'react-router';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Menu from 'material-ui/svg-icons/navigation/menu';
import ViewModule from 'material-ui/svg-icons/action/view-module';
import {white} from 'material-ui/styles/colors';

const Style = StyleSheet.create({
	AppBar: {
		position: 'fixed',
		top: 0,
		overflow: 'hidden',
		maxHeight: 57
	},
	MenuButton: {
		marginLeft: 10
	},
	IconsRightContainer: {
		marginLeft: 20
	}
});

class Header extends React.Component {

	render() {
		const {styles, handleChangeRequestNavDrawer} = this.props;
		return (
			<div>
				<AppBar
					style={{...styles}}
					className={css(Style.AppBar)}
					title={<div/>}
					iconElementLeft={
						<IconButton 
							className={css(Style.MenuButton)} 
							onClick={handleChangeRequestNavDrawer}
						>
							<Menu color={white}/>
						</IconButton>
					}
					iconElementRight={
						<div className={css(Style.IconsRightContainer)}>
							<IconMenu 
								color={white}
								iconButtonElement={
									<IconButton><MoreVertIcon color={white}/></IconButton>
								}
								targetOrigin={{horizontal: 'right', vertical: 'top'}}
								anchorOrigin={{horizontal: 'right', vertical: 'top'}}
							>
								<a href="/logout">
								<MenuItem 
									primaryText="Log out" 
									containerElement={<div/>}
								/>
								</a>
							</IconMenu>
						</div>
					}
				/>
			</div>
		);
	}
}

export default Header;
