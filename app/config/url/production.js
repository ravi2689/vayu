/**
 * Copyright - Panally Internet
 */

const Enum = require('enum');

const ConstUrlsProduction = {
	'CORS' : '',
	'MessageUrl': 'http://api.msg91.com/api/v2/sendsms',
	'OtpUrl': 'http://api.msg91.com/api/sendotp.php',
	'Auth': 'https://business.panally.com/api/auth/',
	'Api' : 'https://api.panally.com/v1/',
	'Static': {
		'Host': 'https://business-assets.panally.com',
		'Font': 'https://business-assets.panally.com/static/fonts/',
		'Media': 'https://business-assets.panally.com/static/img/',
		'Css': 'https://business-assets.panally.com/static/css/',
		'Js': 'https://business-assets.panally.com/static/js/',
		'App': {
			'Endpoint': 'https://business-assets.panally.com/static/img/app/'
		},
	},
	'Fetch': {
		'Index': 'https://business.panally.com/api/index/',
		'Business': 'https://business.panally.com/api/business/',
	},
	'Post': {
		'Register': 'https://business.panally.com/api/register',
		'ValidateRef': 'https://business.panally.com/api/validate-ref',
		'Referral': 'https://business.panally.com/api/referral',
		'ValidateRedeem': 'https://business.panally.com/api/validate-redeem',
		'Redeem': 'https://business.panally.com/api/redeem'
	}
}

module.exports = new Enum(ConstUrlsProduction).toJSON();