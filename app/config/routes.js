/**
 * Copyright - Panally Internet
 */

import React from 'react'

import {Route, Router, IndexRoute, browserHistory} from 'react-router'

import Login from 'app/pages/login/Page'

import App from 'app/pages/App'
import Index from 'app/pages/index/Page'
import Register from 'app/pages/register/Page'
import Referral from 'app/pages/referral/Page'
import Redeem from 'app/pages/redeem/Page'

import FourOhFour from 'app/pages/error/404/Page'

import Contact from 'app/pages/contact/Page'

if (typeof window === 'object') {
    function createElement(Component, props) {
        return <Component {...props} {...window.PROPS} />;
    }
}

const routes = (
	<Router history={browserHistory}>
		<Route path='login' component={Login}/>
		<Route path='/' component={App}>
			<IndexRoute component={Index}/>
			<Route path='register' component={Register}/>
			<Route path='referral' component={Referral}/>
			<Route path='redeem' component={Redeem}/>
			<Route path='contact' component={Contact}/>
		</Route>
		<Route path='oops/'>
			<Route path='404' component={FourOhFour}/>
		</Route>
    </Router>
)

module.exports = {
	routes: routes
}
