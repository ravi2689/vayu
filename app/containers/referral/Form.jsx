/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'

import validate from './validate'

import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

import 'app/assets/css/redux-form.css'

const reducer = combineReducers({
	form: reduxFormReducer
})

const store = createStore(reducer)

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	<label>{label}</label>
	<div>
	<input {...input} type={type} placeholder={label}/>
	{touched && error && <span>{error}</span>}
	</div>
	</div>
)

class FieldArraysForm extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<form 
				onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
				{
					this.props.error 
					&&
					<strong>
					  {this.props.error}
					</strong>
				}
				<Field
					name="phoneNumber"
					type="text"
					component={renderField}
					label="Ph. No."
				/>
				<Field
					name="refPhoneNumber"
					type="text"
					component={renderField}
					label="Referrer's Ph. No."
				/>
				<div>
					<button
						type="button"
						disabled={this.props.pristine || this.props.submitting}
						onClick={this.props.handleSubmit(values => 
							this.props.generateOtp({
								...values
							})
						)}
					>
						Generate OTP
					</button>
				</div>
				<Field
					name="password"
					type="text"
					component={renderField}
					label="OTP"
				/>
				<div>
					<button 
						type="submit" 
						disabled={this.props.pristine || this.props.submitting}
					>
						Submit
					</button>
					<button 
						type="button" 
						disabled={this.props.pristine || this.props.submitting} 
						onClick={this.props.reset}
					>
						Clear Values
					</button>
				</div>
			</form>
		)
	}
}

let successfulSubmits = 0;

const afterSubmit = function(result, dispatch){
	if (successfulSubmits >= 1){
		window.location.replace('/');
	} else {
		successfulSubmits = successfulSubmits + 1;
	}
}

export default reduxForm({
	form: 'fieldArraysReferral',
	onSubmitSuccess: afterSubmit,
	validate
})(FieldArraysForm)
