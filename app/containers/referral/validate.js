/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

const validate = values => {
	const errors = {}
	if (!values || !values.phoneNumber) {
		errors.phoneNumber = 'Required'
	}
	if (!values || !values.refPhoneNumber) {
		errors.refPhoneNumber = 'Required'
	}
	if (values.phoneNumber && !/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i.test(values.phoneNumber)){
		errors.invalidPhoneNumber = 'Invalid Phone Number'
	}
	if (values.refPhoneNumber && !/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i.test(values.refPhoneNumber)){
		errors.invalidRefPhoneNumber = 'Invalid Phone Number'
	}
	return errors
}

export default validate
