/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import Paper from 'material-ui/Paper'
import {white, purple600, purple500} from 'material-ui/styles/colors'
import {LineChart, Line, ResponsiveContainer} from 'recharts'
import {typography} from 'material-ui/styles'

const Style = StyleSheet.create({
	Paper: {
		backgroundColor: [purple500],
		height: 150
	},
	Div: {
		height: 95,
		padding: '5px 15px 0 15px'
	},
	Header: {
		fontSize: 24,
		fontWeight: [typography.fontWeightLight],
		color: 'white',
		backgroundColor: [purple600],
		padding: 10,
	}
});

class DailyRegistrations extends React.Component {
	render() {
		return (
			<Paper className={css(Style.Paper)}>
				<div className={css(Style.Header)}>Daily Registrations</div>
				<div className={css(Style.Div)}>
					<ResponsiveContainer >
						<LineChart data={this.props.data}>
							<Line type="monotone" dataKey="numberOfRegistrations" stroke="#8884d8" strokeWidth={2} />
						</LineChart>
					</ResponsiveContainer>
				</div>
			</Paper>
		)
	}
}

export default DailyRegistrations;
