/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react';
import { StyleSheet, css } from 'aphrodite'

import Paper from 'material-ui/Paper';
import {white, pink600, pink500} from 'material-ui/styles/colors';
import {BarChart, Bar, ResponsiveContainer, XAxis} from 'recharts';
import GlobalStyles from '../../styles';

const Style = StyleSheet.create({
	Paper: {
		backgroundColor: pink600,
		height: 150
	},
	Div : {
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '95%',
		height: 85
	},
	Header : {
		color: white,
		backgroundColor: pink500,
		padding: 10
	}
});

class DailyReferrals extends React.Component {
	render() {
		return (
			<Paper className={css(Style.Paper)}>
				<div style={{...GlobalStyles.title}} className={css(Style.Header)} >Daily Referrals</div>
				<div className={css(Style.Div)}>
					<ResponsiveContainer>
						<BarChart data={this.props.data} >
							<Bar dataKey="numberOfConversions" fill={pink500}/>
							<XAxis dataKey="date" stroke="none" tick={{fill: white}}/>
						</BarChart>
					</ResponsiveContainer>
				</div>
			</Paper>
		);
	}
}

export default DailyReferrals;
