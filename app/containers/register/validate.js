/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

const validate = values => {
	const errors = {}
	if (!values.members || !values.members.length) {
		errors.members = { _error: 'At least one member must be entered' }
	} else {
		const membersArrayErrors = []
		values.members.forEach((member, memberIndex) => {
			const memberErrors = {}
			if (!member || !member.phoneNumber) {
				memberErrors.phoneNumber = 'Required'
				membersArrayErrors[memberIndex] = memberErrors
			}
			if (member.phoneNumber && !/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i.test(member.phoneNumber)){
				memberErrors.phoneNumber = 'Invalid Phone Number'
				membersArrayErrors[memberIndex] = memberErrors
			}
			return memberErrors
		})
		if(membersArrayErrors.length) {
			errors.members = membersArrayErrors
		}
	}
	return errors
}

export default validate
