/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'

import validate from './validate'

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	<label>{label}</label>
	<div>
	<input {...input} type={type} placeholder={label}/>
	{touched && error && <span>{error}</span>}
	</div>
	</div>
)

let isFirst = true;

const renderMembers = function({ fields, meta: { touched, error } }) {
	if(isFirst){
		fields.push({});
		isFirst=false;
	}
	return (
		<ul>
			{fields.map((member, index) =>
				<li key={index}>
					<button
						type="button"
						title="Remove Member"
						onClick={() => fields.remove(index)}/>
					<h4>Member #{index + 1}</h4>
					<Field
						name={`${member}.phoneNumber`}
						type="text"
						component={renderField}
						label="Phone Number"/>
				</li>
			)}
			<li>
				<button type="button" onClick={() => fields.push({})}>Add Member</button>
				{touched && error && <span>{error}</span>}
			</li>
		</ul>
	)
}

class FieldArraysForm extends React.Component {

	render() {
		return (
			<form onSubmit={this.props.handleSubmit}>
				<FieldArray name="members" component={renderMembers}/>
				{
					this.props.error 
					&&
					<strong>
					  {this.props.error}
					</strong>
				}
				<div>
					<button 
						type="submit" 
						disabled={this.props.pristine || this.props.submitting}
					>
						Submit
					</button>
					<button 
						type="button" 
						disabled={this.props.pristine || this.props.submitting} 
						onClick={this.props.reset}
					>
						Clear Values
					</button>
				</div>
			</form>
		)
	}
}

export default reduxForm({
	form: 'fieldArrays',
	validate
})(FieldArraysForm)
