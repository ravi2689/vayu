/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'

import validate from './validate'

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	<label>{label}</label>
	<div>
	<input {...input} type={type} placeholder={label}/>
	{touched && error && <span>{error}</span>}
	</div>
	</div>
)


class FieldArraysForm extends React.Component {

	render() {
		return (
			<form onSubmit={this.props.handleSubmit}>
				{
					this.props.error 
					&&
					<strong>
					  {this.props.error}
					</strong>
				}
				<Field
					name="phoneNumber"
					type="text"
					component={renderField}
					label="Phone Number"
				/>
				<div>
					<button
						type="button"
						disabled={this.props.pristine || this.props.submitting}
						onClick={this.props.handleSubmit(values => 
							this.props.generateOtp({
								...values
							})
						)}
					>
						Generate OTP
					</button>
				</div>
				<Field
					name="password"
					type="text"
					component={renderField}
					label="OTP"
				/>
				<div>
					<button 
						type="submit" 
						disabled={this.props.pristine || this.props.submitting}
					>
						Submit
					</button>
					<button 
						type="button" 
						disabled={this.props.pristine || this.props.submitting} 
						onClick={this.props.reset}
					>
						Clear Values
					</button>
				</div>
			</form>
		)
	}
}

let successfulSubmits = 0;

import { reset } from 'redux-form';

const afterSubmit = function(result, dispatch){
	if (successfulSubmits >= 1){
		window.location.replace('/');
	} else {
		successfulSubmits = successfulSubmits + 1;
	}
}

export default reduxForm({
	form: 'fieldArraysRedeem',
	onSubmitSuccess: afterSubmit,
	validate
})(FieldArraysForm)
