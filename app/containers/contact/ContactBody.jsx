/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'
import Url from 'app/config/core/url'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: '20px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: '15px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			fontWeight: 'bold',
			padding: '10%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: '15px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			fontWeight: 'bold',
			padding: '10%',
		},
	},
	ContactButton : {
		[CoreStyle.PC.BREAKPOINT] :{
			display: 'block',
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			fontWeight : 'bold',
			margin: '2% 30%',
			padding: '1%',
			textAlign: 'center',
			borderRadius: '5px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			fontWeight : 'bold',
			width: '40%',
			margin: 'auto',
			padding: '3%',
			textAlign: 'center',
			height: '45px',
			borderRadius: '5px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			fontWeight : 'bold',
			width: '90%',
			margin: 'auto',
			padding: '3%',
			textAlign: 'center',
			height: '45px',
			borderRadius: '5px',
		},
		backgroundColor: [CoreStyle.COLOR.YELLOW],
		color: [CoreStyle.COLOR.BLACK],
		':hover': {
			cursor: 'pointer',
			backgroundColor: [CoreStyle.COLOR.RED],
			color: [CoreStyle.COLOR.WHITE],
			fontWeight: 'bold',
		},
	},
})

export default class ContactBody extends React.Component {
	render() {
		return (
			<div>
				<div className={css(Style.Wrapper)}>
					You can call/whatsapp us at - <br/> +91-8277405700 <br/> +91-9990426229 <br/><br/>Or, you can mail us at - <br/> team@panally.com<br/><br/><br/>We look forward to hear from you!
				</div>
				<a href='/contact-form' target="_blank" rel="noopener noreferer" className={css(Style.ContactButton)}>
					Fill the contact form
				</a>
			</div>
		)
	}
}
