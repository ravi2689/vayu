/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

const validate = values => {
	const errors = {}
	if (!values.members || !values.members.length) {
		errors.members = { _error: 'At least one member must be entered' }
	} else {
		const membersArrayErrors = []
		values.members.forEach((member, memberIndex) => {
			const memberErrors = {}
			if (!member || !member.username) {
				memberErrors.username = 'Required'
				membersArrayErrors[memberIndex] = memberErrors
			}
			if (!member || !member.password) {
				memberErrors.password = 'Required'
				membersArrayErrors[memberIndex] = memberErrors
			}
			return memberErrors
		})
		if(membersArrayErrors.length) {
			errors.members = membersArrayErrors
		}
	}
	return errors
}

export default validate
