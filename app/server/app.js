/**
 * Copyright - Panally Internet
 */
  
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// var Raven = require('raven');

// // Must configure Raven before doing anything else with it
// Raven.config('https://c50eb02b45e84facb3607b15d109ca7c@sentry.io/145177').install();

// // The request handler must be the first mbusinessIddleware on the app
// app.use(Raven.requestHandler());

// // The error handler must be before any other error mbusinessIddleware
// app.use(Raven.errorHandler());

// Configuring the App
app.set('port', (process.env.PORT || 3000));

if (process.env.NODE_ENV=='development'){
	// Static URL
	app.use('/assets/', express.static(path.resolve('../assets/static/')));
}


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Additional mbusinessIddleware which will set headers that we need on each request.
app.use(function(req, res, next) {
		// Set permissive CORS header - this allows this server to be used only as
		// an API server in conjunction with something like webpack-dev-server.
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

		// Disable caching so we'll always get the latest comments.
		res.setHeader('Cache-Control', 'no-cache');
		next();
});

/******************
 *
 *
 * AUTH BEGIN
 *
 *
 ******************/

const passport = require('passport');
const Strategy = require('passport-local').Strategy;
const request = require('superagent');
const Url = require('app/config/core/url');

passport.use(new Strategy(
	function(username, password, cb) {
		const authUrl = Url.Auth + username + '/' + password;
		request
		.get(authUrl)
		.end(function(error, response){
			if (error) {
				return cb(error);
			}
			if (!response.body.businessId || !response.body.username){
				return cb(null, false);
			}
			return cb(null, response.body)
		});
	})
);

passport.serializeUser(function(user, cb) {
	cb(null, user.businessId);
});


passport.deserializeUser(function(businessId, cb) {
	const fetchBusiness = Url.Fetch.Business + businessId;
	request
	.get(fetchBusiness)
	.end(function(error, response){
		if (error) {
			cb(error);
		}
		if (!response.body.businessId){
			return cb(null, false);
		}
		cb(null, response.body)
	});
});

app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));


app.use(require('cookie-session')({
	name: 'session',
	secret: 'keyboard cat',
	// Cookie Options
	maxAge: 7 * 24 * 60 * 60 * 1000
}))

// Initialize Passport and restore authentication state, if any, from the session.
app.use(passport.initialize());
app.use(passport.session());

// Define routes.
app.post('/login',
	passport.authenticate('local', {
		failureRedirect: '/login' 
	}),
	function(req, res) {
		res.status(200).send();
});
  
app.get('/logout', function(req, res){
	req.logout();
	res.redirect(302,'/');
});

/******************
 *
 *
 * AUTH END
 *
 *
 ******************/

 // Connecting to the app router
var router = require('app/router/main-router');
app.use('/', router);

//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){
	res.redirect(302,'/oops/404');
});

// Debug when the server starts
app.listen(app.get('port'), function() {
	console.log('Server started: http://localhost:' + app.get('port') + '/');
});

module.exports = app;